﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{

    PlayerInput KeyInput;
    public int Ybounds = 15;
    public int Xbounds = 15;

    void Awake()
    {
        KeyInput = GetComponent<PlayerInput>();
    }
	// Use this for initialization
	void Start () 
    {
	
	}


	// Update is called once per frame
	void Update () 
    {
//#if UNITY_EDITOR
        if (Input.GetAxis("Mouse ScrollWheel") > 0) Camera.main.orthographicSize /= KeyInput.ScrollSpeed;
        if (Input.GetAxis("Mouse ScrollWheel") < 0) Camera.main.orthographicSize *= KeyInput.ScrollSpeed;

        if (KeyInput.MoveY > 0 && (int)transform.position.y < Ybounds) transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, KeyInput.MoveY, 0), Time.deltaTime);
        else if (KeyInput.MoveY < 0 && (int)transform.position.y > -Ybounds) transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, KeyInput.MoveY, 0), Time.deltaTime);

        if (KeyInput.MoveX > 0 && (int)transform.position.x < Xbounds) transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(KeyInput.MoveX, 0, 0), Time.deltaTime);
        else if (KeyInput.MoveX < 0 && (int)transform.position.x > -Xbounds) transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(KeyInput.MoveX, 0, 0), Time.deltaTime);

//#endif



	}


}
