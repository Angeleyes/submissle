﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchControl : MonoBehaviour
{

    PlayerInput KeyInput;
    public LayerMask touchInputMask;
    internal Camera MainCamera;
    List<GameObject> TouchList;
    GameObject[] OldTouches;
    internal Ray ray;
    internal RaycastHit hit;
    public int DragDeadZone = 3;
    public Touch Cameratouch;
    Vector2 TouchInitialPosition;


    internal delegate void TouchOperation(Touch t, GameObject g);

    TouchOperation DoTouchOperation;

    public float ZoomSpeed = 0.000001f;

    private int Ybounds = 15;
    private int Xbounds = 15;

    int Deltaypos;
    int Deltaxpos;

    void Awake()
    {

        KeyInput = GetComponent<PlayerInput>();
        TouchList = new List<GameObject>();
        MainCamera = GetComponent<Camera>();
        DoTouchOperation += CheckBlockTouch;
        // Ybounds = GameManager.Board / 2;
        // Xbounds = Ybounds;
    }

    void Update()
    {

        #region mouse
        //#if UNITY_EDITOR


        if (Input.GetAxis("Mouse ScrollWheel") > 0) Camera.main.orthographicSize /= KeyInput.ScrollSpeed;
        if (Input.GetAxis("Mouse ScrollWheel") < 0) Camera.main.orthographicSize *= KeyInput.ScrollSpeed;



        if (KeyInput.MoveY > 0 && (int)transform.position.y < Ybounds) transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, KeyInput.MoveY, 0), Time.deltaTime);
        else if (KeyInput.MoveY < 0 && (int)transform.position.y > -Ybounds) transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(0, KeyInput.MoveY, 0), Time.deltaTime);

        if (KeyInput.MoveX > 0 && (int)transform.position.x < Xbounds) transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(KeyInput.MoveX, 0, 0), Time.deltaTime);
        else if (KeyInput.MoveX < 0 && (int)transform.position.x > -Xbounds) transform.position = Vector3.Lerp(transform.position, transform.position + new Vector3(KeyInput.MoveX, 0, 0), Time.deltaTime);


        if (Input.GetMouseButtonDown(0))
        {

            ray = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, touchInputMask))
            {
                GameObject HitObject = hit.transform.gameObject;

                if (Input.GetMouseButtonDown(0))
                {
                    HitObject.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
                    //DoTouchOperation();
                }

            }

        }


//#endif
        #endregion


        CheckForTouch();
        CheckForPinch();
        CheckForPan();
        //CheckForCameraMove();

    } // end of update

    
    public bool ToggleTouchMode(bool TouchMode)
    {
        Debug.Log("toggeling touch mode");
        return !TouchMode;
    }

    IEnumerator ChangeTouchOperation()
    {

        DoTouchOperation -= CheckBlockTouch;
        DoTouchOperation += CheckMissleTouch;
        yield return new WaitForSeconds(0.5f);

    }

    public void CheckForPan()
    {
        if(Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Vector2 ChangedPos = Input.GetTouch(0).deltaPosition;
            transform.Translate(-ChangedPos.x * 0.2f * Time.deltaTime, -ChangedPos.y * 0.2f * Time.deltaTime, 0);
        }
    }

    public void CheckForTouch()
    {
        if (Input.touchCount > 0)
        {

            OldTouches = new GameObject[TouchList.Count];
            TouchList.CopyTo(OldTouches);
            TouchList.Clear();


            foreach (Touch touch in Input.touches)
            {
                ray = MainCamera.ScreenPointToRay(touch.position);

                if (Physics.Raycast(ray, out hit, touchInputMask))
                {
                    GameObject HitObject = hit.transform.gameObject;
                    TouchList.Add(HitObject);

                    //Debug.Log("doing touch operation...");
                    DoTouchOperation(touch, HitObject);
                    //CheckBlockTouch(touch, HitObject);

                }

            }// end of foreach
        }
    }
        //foreach (GameObject g in OldTouches)
        //{
        //    if (!TouchList.Contains(g))
        //    {
        //        g.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
        //    }

        //}
    

    public void CheckBlockTouch(Touch touch, GameObject HitObject)
    {
        switch (touch.phase)
        {
            case TouchPhase.Began:
                {
                    TouchInitialPosition = touch.position;
                    HitObject.SendMessage("OnTouchDown", hit.point, SendMessageOptions.DontRequireReceiver);
                }
                break;
            case TouchPhase.Ended:
                {
                    HitObject.SendMessage("OnTouchUp", hit.point, SendMessageOptions.DontRequireReceiver);
                }
                break;
            case TouchPhase.Stationary:
                {
                    HitObject.SendMessage("OnTouchStay", hit.point, SendMessageOptions.DontRequireReceiver);
                }
                break;
            case TouchPhase.Moved:
                {
                    //if (HitObject is TouchScroll) HitObject.GetComponent<TouchScroll>().Direction = touch.position;
                    HitObject.SendMessage("OnTouchMoved", hit.point, SendMessageOptions.DontRequireReceiver);
                }
                break;
            case TouchPhase.Canceled:
                {
                    HitObject.SendMessage("OnTouchExit", hit.point, SendMessageOptions.DontRequireReceiver);
                }
                break;

            default:
                {
                    Debug.Log("something wrong in touch control switch");
                }
                break;

        } // end of switch
    }

    public void CheckMissleTouch(Touch touch, GameObject HitObject)
    {
        GameManager.ShootMissle(HitObject.transform.position, "default");
        DoTouchOperation -= CheckMissleTouch;
        DoTouchOperation += CheckBlockTouch;
    }

    public void CheckForPinch()
    {
        if (Input.touchCount == 2)
        {
            Touch Touch1 = Input.GetTouch(0);
            Touch Touch2 = Input.GetTouch(1);

            Vector2 Touch1PrevPos = Touch1.position - Touch1.deltaPosition;
            Vector2 Touch2PrevPos = Touch2.position - Touch2.deltaPosition;

            float PrevTouchDelta = (Touch1PrevPos - Touch2PrevPos).magnitude;
            float TouchDelta = (Touch1.position - Touch2.position).magnitude;

            float TouchDifference = PrevTouchDelta - TouchDelta;

            if (MainCamera.orthographic)
            {
                MainCamera.orthographicSize += (TouchDifference/4) * ZoomSpeed;
                MainCamera.orthographicSize = Mathf.Max(MainCamera.orthographicSize, 0.5f);
            }
            else
            {
                MainCamera.fieldOfView += ZoomSpeed;
                MainCamera.fieldOfView = Mathf.Clamp(MainCamera.fieldOfView, 0.1f, 179.9f);
            }

        }
    }


    public void CheckForCameraMove()
    {
    //    if(Input.touchCount == 1)
    //    {
    //        Cameratouch = Input.GetTouch(0);
    //        if (Cameratouch.phase == TouchPhase.Moved)
    //        {
    //            float Dif = Cameratouch.position.magnitude - Cameratouch.deltaPosition.magnitude;

    //            if (Dif >= 99f)
    //            {
    //                camera.transform.position = Vector2.Lerp(camera.transform.position, Cameratouch.position, Time.deltaTime);
    //            }
    //        }

                    //        {
                    //            TouchDirection = touch.position - TouchInitialPosition;

                    //            if (TouchDirection.magnitude > DragDeadZone)
                    //            {
                    //                //if the difference in initial position and moved position is large enough
                    //                //move the camera in that direction
                    //                camera.transform.position = Vector3.Lerp(camera.transform.position, touch.position, Time.deltaTime);
                    //            }
                    //            Debug.Log("is this a drag??");

        //}
    }


}
       
