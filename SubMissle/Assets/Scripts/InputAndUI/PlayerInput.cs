﻿using UnityEngine;
using System.Collections;

public class PlayerInput : MonoBehaviour 
{
    public static PlayerInput PlayersInput;

    public float Ysensitivity = 5;
    public float Xsensitivity = 5;

    public float ScrollSpeed = 1.05f;

    internal float MoveY { get; private set;}
    internal float MoveX { get; private set;}


    //public bool PlayerClick { get; private set; } // not working for w/e reason, had to put it into the block.cs itself

	
	// Update is called once per frame
	void Update () 
    {

        MoveY = Input.GetAxis("Vertical") * Ysensitivity; // w and s

        MoveX = Input.GetAxis("Horizontal") * Xsensitivity; // a and d

    
        //PlayerClick = Input.GetMouseButtonDown(0);
	
	}
}
