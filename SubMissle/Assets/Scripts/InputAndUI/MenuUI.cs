﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class MenuUI : MonoBehaviour {


    //public Canvas MoreMenu;
    public Button Continue;
    public Button More;
    public Button Back;
    public Button Exit;

    public List<GameObject> SubClips;
    public MovieScript MovieScreenObject;

    public AudioSource Thunder1;
    public MusicFader TitleMuze;
    public MusicFader OceanFx;
    public Camera MainCamref;
    public bool FadingIn;



    public GameObject LevelSelectHandle;

    public float BlockFadeSpeed;
    bool InContinue;
    //Canvas Menu;
    public Text Credits;
    public GameObject CreditHandle;
    bool SlideOn;
    public float SliderPosition;




    public int FromLeft; //-17
    public int FromRight; //-10
    float From;

    Animator MenuAnim;

    //int Timer = 0;
    public GameObject BackGroundBlocks;

	// Use this for initialization
	void Start () 
    {
        Continue = Continue.GetComponent<Button>();
        More = More.GetComponent<Button>();
        Exit = Exit.GetComponent<Button>();

        MenuAnim = GetComponent<Animator>();
        Back = Back.GetComponent<Button>();
        //LevelSelectHandle.SetActive(false);
        InContinue = false;
        From = FromRight;
        SliderPosition = 1;
        Back.enabled = false;
        FadingIn = false;
	}


	public void PlaySubClip(int ClipIndex)
	{
		//MainCamref.cullingMask = 0; //must always do this before flash effect....kinda dumb
		//Invoke("FlashEffect", 0.05f);




#if UNITY_ANDROID || UNITY_IOS

		Debug.Log("insdie menu...calling mobile movie player playing moive: " + SubClips[ClipIndex].GetComponent<MovieScript>().MovieName);
        Handheld.PlayFullScreenMovie(SubClips[ClipIndex].GetComponent<MovieScript>().MovieName, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.Fill);
#endif

		//#if UNITY_EDITOR_WIN || UNITY_EDITOR_OSX

		//Debug.Log("insdie menu...calling unity editor movie player");
		//MovieScreenObject.gameObject.SetActive(true);
		StartCoroutine(IOfadeLevelSelect(SubClips[ClipIndex].GetComponent<MovieScript>().ClipDuration));
		//MovieScreenObject.SetandPlay(SubClips[ClipIndex].GetComponent<MovieScript>().MovieScene, 1);
		//#endif


		//if (SystemInfo.deviceType == DeviceType.Handheld) Handheld.PlayFullScreenMovie(SubClips[ClipIndex].name, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.Fill);
		//else
		//{
		//    Debug.Log("playing subclip");
		//    MovieScreenObject.SetandPlay(SubClips[ClipIndex], 1);
		//}
	}


    IEnumerator IOfadeLevelSelect(float ClipDuration)
    {
		TitleMuze.FadeOutMusic();
        float timer = 0;

        LevelSelectHandle.SetActive(false);

        while (ClipDuration > timer)
        {
            timer += Time.deltaTime;
            yield return null;
        }
        Debug.Log("debug3");
        yield return new WaitForSeconds(1f);
        LevelSelectHandle.SetActive(true);

        MenuAnim.SetTrigger("FadeToLevels");
		TitleMuze.FadeInMusic();

        Debug.Log("debug4");

        yield return null;

    }

    public void MorePress()
    {
        From = FromRight;
        MenuAnim.SetTrigger("FadeOut");
        More.enabled = false;
        Continue.enabled = false;
        BlockFadeSpeed = 0.07f; // oh no more magic numbers :(
        StartCoroutine("BlockFade");
        Back.enabled = true;
        Credits.enabled = true;
        //StartCoroutine("Slider");
        MenuAnim.SetTrigger("SubMenuFadeIn");
        //start slider coroutine

    }


    //IEnumerator Slider()
    //{
    //    while (true) //while slider is enabled
    //    {
    //        //Slide(SliderPosition);
            
    //        yield return null;
    //    }

    //}

    public void Slide(float delta)
    {
        SliderPosition = delta;
    }


    public void BackPress()
    {
        From = FromRight;
        MenuAnim.SetTrigger("FadeIn");
        StopCoroutine("Slider");
        Back.enabled = false;
        More.enabled = true;
        Continue.enabled = true;
        
        if (InContinue)
        {
            From = -13.047f; // ye ye ye magic numbers i know
            BlockFadeSpeed = 0.2f; // again..
            StartCoroutine("BlockFade");
            MenuAnim.SetTrigger("FadeIn");
            LevelSelectHandle.SetActive(false);
            InContinue = false;
            //LevelSelectHandle.GetComponent<CanvasGroup>().alpha = 0;
        }
        else
        {
            
            StartCoroutine("UnBlockFade");
            InContinue = false;
        }
        
    }




    IEnumerator BlockFade() // race conditions with the unblockfade...causing fps lag
    {
        StopCoroutine("UnblockFade");
        while(BackGroundBlocks.transform.position.z < From + 3) // bug fix, later
        {
            BackGroundBlocks.transform.position = Vector3.Lerp(BackGroundBlocks.transform.position, new Vector3(0, 0, 10), BlockFadeSpeed * Time.deltaTime);
            yield return null;           
        }
        if(InContinue) InContinue = false;
    }

    IEnumerator UnBlockFade() // for the back button in the more sub menu
    {
        StopCoroutine("BlockFade");
        while(BackGroundBlocks.transform.position.z > From) // fadingintrue
        {
            BackGroundBlocks.transform.position = Vector3.Lerp(BackGroundBlocks.transform.position, new Vector3(0, 0, From-1), 0.9f * Time.deltaTime);
            yield return null;
        }
        //FadingIn = false;
    }


    public static int CurrentLevel = 0;
    public void LoadLevel(int index)
    {
        CurrentLevel = index;
        Thunder1.volume = 1;
        Thunder1.Play();
        OceanFx.FadeOutMusic();
        MainCamref.GetComponent<TitleBackGround>().clip.volume = 0;
        MainCamref.GetComponent<TitleBackGround>().clip2.volume = 0;
        LevelSelectHandle.gameObject.SetActive(false);
        MainCamref.cullingMask = 0;
        Invoke("FlashEffect", 0.05f);
        TitleMuze.FadeOutMusic();


        Invoke("LoadLevel", 2);
    }

    void FlashEffect()
    {
        MainCamref.cullingMask = 1;
    }

    void LoadLevel()
    {
        SceneManager.LoadScene(CurrentLevel);
    }

    public void ContinuePress()
    {
        From = FromLeft;
        StartCoroutine("UnBlockFade");
        InContinue = true;
        MenuAnim.SetTrigger("FadeOut");
        More.enabled = false;
        Continue.enabled = false;
        Back.enabled = true;
        LevelSelectHandle.SetActive(true);
        //Back.GetComponent<Text>().enabled = false;
        Credits.enabled = false; // change later
        MenuAnim.SetTrigger("FadeToLevels");
        //LevelSelectHandle.SetActive(true);

    }


    public void ExitGame()
    {
        Application.Quit();
    }

}
