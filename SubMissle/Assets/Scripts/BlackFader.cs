﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BlackFader : MonoBehaviour {


    //static bool Switch = false;

    public void Fade(int delta, int time)
    {
        SwitchActive(true);
        delta = Mathf.Clamp(delta, 0, 1);
        StartCoroutine(Wait(time, delta));
    }

    IEnumerator Wait(int t, int delta)
    {
        gameObject.GetComponent<Image>().CrossFadeAlpha(delta, t, false);
        yield return new WaitForSeconds(t+1);
        SwitchActive(false);
    }

    private void SwitchActive(bool value)
    {
        gameObject.SetActive(value);
    }


}
