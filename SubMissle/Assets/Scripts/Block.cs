﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class Block : MonoBehaviour
{
    /* <copyright file = Block.cs --- Company: Ogrebeast Software SP>
   Copyright (c) 2015 All Rights Reserved
   <copyright> See License Below
   <author> Connor Jensen 
   <date> 9/4/2015
   <summary> Complete functions and attributes for the Block.cs class written in c# (c sharp) for the touch device game: Submissle */

    //====================================================================================================================================//

    /*
     * Copyright 2015 Connor Jensen

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
     */


    // refactor this into super class and have seperate youblock and enemyblock classes

    public Color LivingColor;
    public bool Dead;
    public bool Blessed;

    Vector3 originScale;

    public bool IsEnemy;
    public float PopPercent = 0.15f;

    public string BlockTag = "NO TAG";
    public int ChildrenPopulation;

    internal Block MainParentBlock;

    public int AdjNeighborCount;
    public Block EnemyTarget;

    //attributes
    public float Health = 350;
    public int GeneHealth = 250; // the health that block has and will pass on to its children
    public int MaxHealth = 350;

    public float Tenacity;
    public int ReproductionChance;
    public int Skill;
    public bool isFighting;
    public int AttackSpeed;

    public Vector2 BlockPosition;

    //public List<Vector2> EmptyNeighborsPos;
    public List<Vector2> NeighborPosList;
    //public List<Vector2> LiveNeighborsPos;
    //public List<Vector2> EnemyNeighborsPos;

    public List<Block> Neighbors; // DO NOT CLEAR EVER
    public List<Block> DeadNeighbors;
    public List<Block> LiveNeighbors;
    public List<Block> EnemyNeighbors;
    public Stack<Block> EnemyStack;

    internal SpriteRenderer rend;

    public int B_ReproMax = 120;
    public int B_HpLimit = 500;
    public float B_HpLossRate = 0.75f;
    public int B_HpBlessBoost = 100;
    public int B_Heal = 50;
    public float B_TenLimit = 10;
    public int B_ReproThres = 140;


       // HealthLossRate = 0.75f;
        //BlockHp = 350;
        //BlockGenes = 250;
        //HealingTouch = 50;

    ////inspector variables
    //public int ReproductionMax; // 650
    //public int HealthLimit; // 500
    //public float SpiritRate; // rate in which spirit increases  0.01
    //public int HealthLossRate; // rate in which hp is lost each cycle 0.025 (change from 0.025 to 1)
    //public int HealthBoost; // 50 atm
    //public static int HealingTouch; // amount healed when you touch/tap a block 2
    //public int TenacityLimit; // thres at which a block will die if gives birth
    //public int ReproductionThreshold; // reproduction threhold to pass inorder to trigger a reproduce 200
    //public static int BlockGenes;
    //public static int BlockHp;


    //int B_ReproInfluence;
    int B_Population;
    public int BlockID;

    internal int ImpactValueR;


    public enum Btype
    {
        YouMain,
        EMain,
        Block
    }

    public Btype BlockType;


    public void InitializeBlock(Vector2 Bp, Btype Bt)
    {
        BlockPosition = Bp;
        BlockType = Bt;
        Dead = true;
    }

    void Awake()
    {
        rend = GetComponent<SpriteRenderer>();
        originScale = transform.localScale;
        EnemyStack = new Stack<Block>();

    }

    virtual public void Start()
    {
        //LivingColor = Color.black;
    }


    //void FixedUpdate()
    //{
    //    if (!Dead && !(this is TideBlock)) LifeCycle();
    //}


    public void BlockUpdate()
    {
        LifeCycle();
    }

    //######################################################################################################################################

    virtual public void ComeToLife(Color c, float hp, Block parent, bool isenemy, string Bt) // pass in ai, if you blocks then ai is null
    {
        //B_ReproInfluence = B_Population;s
        if (this == null) return;

        Dead = false;
        IsEnemy = isenemy;     
        tag = Bt;       
        BlockTag = Bt;
        BlockID = parent.BlockID;
        //NOTE: Might want to clear all neighbor lists here before init Neighbors or do it on death

        InitializeNeighbors();

        MainParentBlock = parent;
        
        Health = (int)Random.Range(300, hp);
        MaxHealth = B_HpLimit;
        Tenacity = Random.Range(1f, 5f);
        ReproductionChance = (int)Random.Range(1, B_ReproMax); // - B_ReproInfluence*0.05f KNOWN BUG 9/11/2015.......blocks wont reproduce atm
        Skill = (int)Random.Range(25, 50);
        LivingColor = c;

        //Signalbirth is not working at the moment, and is the problem for alot of stuff right now
        //SignalBirth();
        rend.material.color = LivingColor;

        GameManager.BlockUpdateList.Add(this);
        MainParentBlock.ChildrenPopulation++;

    }

    //######################################################################################################################################
    //something to note about this function....you really need to set the enemy and dead bools before calling this
    public void InitializeNeighbors()
    {
        //Debug.Log("calling init neighbors for block " + BlockPosition);
        //THIS FUNCTION DOES NOT TELL THE OTHER DEAD NEIGHBORS, THAT IS THE BUG
        //LiveNeighbors.Clear(); //this was added 2/4/2017 and might be causing the adj neighbor bug
        foreach (Block B in Neighbors) 
        {
            if (B.Dead)
            {
                DeadNeighbors.Add(B);
                continue;
            }
            else if (IsEnemy != B.IsEnemy)
            {
                EnemyNeighbors.Add(B);
                //DeadNeighbors.Remove(B);
                B.EnemyNeighbors.Add(this);
                B.DeadNeighbors.Remove(this);
                EnemyStack.Push(B);
                B.EnemyStack.Push(this);
            }
            else
            {
                LiveNeighbors.Add(B);
                B.LiveNeighbors.Add(this);
                B.DeadNeighbors.Remove(this);
            }
            //else { }
            //else Debug.Log("error in spawnchild initilize neighbors...my block type is: " + B.Type.ToString());
            B.AdjNeighborCount = B.LiveNeighbors.Count + B.EnemyNeighbors.Count;
        }
        AdjNeighborCount = LiveNeighbors.Count + EnemyNeighbors.Count;
        
    }

    //######################################################################################################################################

    void SignalDeath() // consider clearing the liveneighbors and dead neighbors lists of Block B 
    {

        //dead neighbors are getting added multiple times and adjneighbor count is wrong
        foreach (Block B in LiveNeighbors)
        {
            B.LiveNeighbors.Remove(this);
            B.DeadNeighbors.Add(this);
            B.AdjNeighborCount = B.LiveNeighbors.Count + B.EnemyNeighbors.Count;
        }
        foreach (Block E in EnemyNeighbors)// have to tell enemies as well
        {
            E.EnemyNeighbors.Remove(this);
            E.DeadNeighbors.Add(this);
            E.AdjNeighborCount = E.LiveNeighbors.Count + E.EnemyNeighbors.Count;
        }
    }

    //######################################################################################################################################

    void SignalBirth() 
    {
        foreach (Block B in LiveNeighbors)
        {
            B.DeadNeighbors.Remove(this);
            Debug.Log("Inside signal birth, telling block " + B.BlockPosition + " and foe: " + B.IsEnemy + "that this block ( " + BlockPosition + " ) is alive" + "and foe of this: " + IsEnemy );
            if (IsEnemy != B.IsEnemy) //enemies add eachother 
            {
                Debug.Log("signaling enemy :" + B.BlockPosition + " that block: " + this.BlockPosition + " is an enemy");
                EnemyNeighbors.Add(B);
                B.EnemyNeighbors.Add(this); //this line is not getting called 2/3/2017
            }
            else
            {
                B.LiveNeighbors.Add(this);
            }

            B.AdjNeighborCount = B.LiveNeighbors.Count + B.EnemyNeighbors.Count;
        }
        AdjNeighborCount = LiveNeighbors.Count + EnemyNeighbors.Count;
    }


    int CountNeighbors()
    {
        return LiveNeighbors.Count + EnemyNeighbors.Count;
    }

    //######################################################################################################################################


    void OnTouchDown()
    {
        if (GameManager.MissileMode && Dead)
        {
            Debug.Log("inside misslemode touch down@@@@@");
            GameManager.ShootMissle(BlockPosition, tag);
        }
        else Touched(); // to avoid the bad code above, instead of calling game manager, create a missle touch mode only activated when the missle button is hit
                        // that way, i can changed Touched() into a delegate that gets changed when the sub button is hit.  


        //Touched();

    }

    void OnTouchDrag()
    {
        //move force reproduce here maybe for testing better /new and improved controls
    }

    //######################################################################################################################################
    virtual public void LifeCycle()
    {
        if (CountNeighbors() < 8) Health -= B_HpLossRate;
        if ((CountNeighbors() == 8 && !Dead) && EnemyStack.Count > 0)
        {
            Fight();
            return;
        }


        if (Health <= 0 && !Dead)
        {
            Die();
            return;
        }

        // the section below can be re-written better to save cycles

        if (CountNeighbors() > 8 || LiveNeighbors.Count > 8)
        {
            rend.material.color = Color.red;
            Debug.Log("error in lifecycle, neighbor count overflow :" + LiveNeighbors.Count + " ...adj: " + AdjNeighborCount + "...enemycount: " + EnemyNeighbors.Count);
        }

        if (EnemyStack.Count > 0)
        {
            Fight();
        }

        //if (IsInvoking()) return;
        //else Invoke("TryRep", 1f);

        if (Random.Range(1, 100) >= 95 && CountNeighbors() < 8) // difference % chance to try to reproduce(5% chance atm)
        {
            if (((Random.Range(1, 100) + ReproductionChance) - (MainParentBlock.ChildrenPopulation * PopPercent)) >= B_ReproThres)
            {
                Reproduce();
            }
        }
    } // end of lifecycle

    //######################################################################################################################################

    public void TryRep()
    {
        if (Random.Range(1, 100) >= 15 && AdjNeighborCount < 8) // difference % chance to try to reproduce(85% chance atm)
        {
            if (((Random.Range(1, 100) + ReproductionChance) - (MainParentBlock.ChildrenPopulation * PopPercent)) >= B_ReproThres)
            {
                Reproduce();
            }
        }
    }

        IEnumerator TryReproduce()
        {

        if (Random.Range(1, 100) >= 80 && AdjNeighborCount < 8) // difference % chance to try to reproduce(20% chance atm)
        {
            if (((Random.Range(1, 100) + ReproductionChance) - (MainParentBlock.ChildrenPopulation * PopPercent)) >= B_ReproThres)
            {
                Reproduce();
            }
        }

        yield return new WaitForSeconds(1f);
        }



    virtual public void Die()
    {
        //AdjNeighborCount = LiveNeighbors.Count + EnemyNeighbors.Count;
        if (CountNeighbors() >= 8 && DeadNeighbors.Count == 0 && EnemyNeighbors.Count == 0)
        {
            Health = MaxHealth;
            return;
        }


        Dead = true;
        Health = 0;
        MaxHealth = 0;
        Tenacity = 0;
        ReproductionChance = 0;
        Skill = 0;
        Blessed = false;
        IsEnemy = false;
        tag = "Block";
        EnemyTarget = null;

        //AdjNeighborCount = 0;
        SignalDeath(); // must call this before clearing the lists below
        AdjNeighborCount = 0;

        EnemyNeighbors.Clear();
        LiveNeighbors.Clear();
        DeadNeighbors.Clear();
        EnemyStack.Clear();
        StopAllCoroutines();
        rend.material.color = Color.white;

        GameManager.BlockUpdateList.Remove(this);

        MainParentBlock.ChildrenPopulation--;      
    }

    //######################################################################################################################################

    public void Reproduce()
    {
        if (CountNeighbors() < 8 && !Dead)
        {
            if (DeadNeighbors.Count == 0 || MainParentBlock == null) return; // safety net
            Block ChildB = GetRandomNeighbor();

            try {
                    if (!ChildB.Dead) return; // if selected pos block is alive, return...just a safety net, should not occur
                }
            catch
            {
                Debug.Log("this happend again....why is htis happeneing with VECTOR: " + ChildB.BlockPosition);
                rend.color = Color.yellow;
            }

            ChildB.ComeToLife(LivingColor, MaxHealth, MainParentBlock, IsEnemy, tag);

            if (Tenacity <= B_TenLimit && CountNeighbors() < 8 && Health < 20) // the force of giving birth kills the weak
            {
                Die();
                //Gm.DeathsDueToLowTenacity++;
            }
        }
    }


    //######################################################################################################################################

    public void Fight()
    {
        EnemyTarget = EnemyStack.Pop();
        #region DebugStuff
        //if(EnemyTarget.IsEnemy == IsEnemy && !EnemyTarget.Dead && !Dead)
        //{
        //    Debug.Log("SOMEHOW, same sides got added to enemy neighbors list... " + BlockPosition + "....has added block: " + EnemyTarget.BlockPosition);
        //    EnemyTarget.Flip();
        //    Spin();
        //}
        //if (!EnemyNeighbors.Contains(EnemyTarget) && !EnemyTarget.Dead && !Dead) //if this block doesnt contain the enemytarget in the list
        //{
        //    Debug.Log("this somehow happened1 and the enemy death is: " + EnemyTarget.Dead + "...and this block is death is: " + Dead);
        //    //EnemyNeighbors.Add(EnemyTarget);
        //}
        //if (!EnemyTarget.EnemyNeighbors.Contains(this) && !Dead && !EnemyTarget.Dead) //if the enemy targets list does not contain this block
        //{
        //    Debug.Log("this somehow happened2 and the enemy death is: " + EnemyTarget.Dead + "...and this block is death is: " + Dead);
        //    //EnemyTarget.EnemyNeighbors.Add(this);
        //}
        //StartCoroutine(Fighting());
        #endregion

        if (EnemyTarget.Health <= 0 || EnemyTarget.Dead || Health <= 0 || EnemyTarget == null)
        {
           // isFighting = false;
            EnemyTarget = null;
            return;
            //StopCoroutine(Fighting());
        }
        else
        {
            if (Health <= (0.15 * MaxHealth) && Tenacity > EnemyTarget.Tenacity) // if hp is low, but tenacity is higher than enemy
            {
                Health += (int)(Tenacity * 5);
                //Debug.Log("this happened.");
            }
            //Debug.Log("block " + BlockPosition + " is about to hit " + EnemyTarget.BlockPosition + " hp: " + EnemyTarget.Health + " for " + Skill + " damage");
            EnemyTarget.Health -= (int)(Skill);
            EnemyTarget.ColorFlash(Color.yellow, 50f);
            //Debug.Log(BlockPosition + " has hit " + EnemyTarget.BlockPosition + " hp: " + EnemyTarget.Health +" for " + Skill + " damage");

            if (EnemyTarget.Health <= 0)
            {
                isFighting = false;
                EnemyTarget = null;
                return;
            }
            else
            {
                EnemyStack.Push(EnemyTarget); //if didnt kill enemy, push them back onto the stack
                //Debug.Log(BlockPosition + " is re-pushing " + EnemyTarget.BlockPosition + " onto the stack, size is now: " + EnemyStack.Count);
            }
        }
    }

    IEnumerator Fighting()
    {
        while ((!Dead || !EnemyTarget.Dead) && isFighting) // bit operators are weird with while loops, look out for possible bug
        {
            if (EnemyTarget.Health <= 0 || EnemyTarget.Dead || Health <= 0 || EnemyTarget == null)
            {
                isFighting = false;
                EnemyTarget = null;
                StopCoroutine(Fighting());
                break;           
            }
            else
            {
                if (Health <= (0.15 * MaxHealth) && Tenacity > EnemyTarget.Tenacity) // if hp is low, but tenacity is higher than enemy
                {
                    Health += (int)(Tenacity * 5);
                    //Debug.Log("this happened.");
                }
                EnemyTarget.Health -= (int)(Skill); 
            }

            yield return new WaitForSeconds(0.5f);
        } // end of while

        //BUG FOUND: NEED TO REMOVE FROM E NEIGHBORS WHEN ENEMY DIES....maybe

    } // end of fighting co-routine


    //IEnumerator ChangeColor(Color NewColor)
    //{
    //    while (rend.color != NewColor)
    //    {
    //        rend.color = Color.Lerp(rend.color, NewColor, Time.deltaTime * 0.5f);
    //        Debug.Log("inside color chagne");
    //        yield return null;
    //    }
    //    yield return null;

    //    //while(rend.color != this.LivingColor)
    //    //{
    //    //    rend.color = Color.Lerp(rend.color, LivingColor, Time.deltaTime * 90f);
    //    //    yield return null;
    //    //}

    //}


    //######################################################################################################################################

    virtual public void Touched()
    {

        if (!Dead && tag == "Block") //temporary bug with cyan colored blessed blocks...its coo is coooo
        {
            ForceReproduce(3);
            Bless();

            Health += B_Heal;
            ColorFlash(Color.blue, 15f);
            Health = Mathf.Clamp(Health, 0, MaxHealth);

            foreach (Block N in LiveNeighbors)
            {
                if (N is BlockMain) continue;
                N.ColorFlash(Color.blue, 15f);
                N.Bless();
                N.Health += B_Heal;
                N.Health = Mathf.Clamp(N.Health, 0, N.MaxHealth); //math clamp f  
                //N.rend.color = Color.blue;  

            }
        }

    }

    public void Bless()
    {
        if (Blessed) return;

        Health += B_HpBlessBoost;
        MaxHealth += (int)Health;
        Skill *= 3;
        ReproductionChance += 50;
        Blessed = true;
    }

    public void ColorFlash(Color c, float speed)
    {
        if (c == Color.blue)
        {
            StopCoroutine(FlashColor(Color.yellow, 50f));
            StopCoroutine(FlashColor(c, speed));
            rend.material.color = LivingColor; //BUG: this causes mains to turn to their child color
            //ColorCoroutineRunning = false;
            StartCoroutine(FlashColor(c, speed));
        }
        else if (ColorCoroutineRunning == false) StartCoroutine(FlashColor(c, speed));
        else return;
    }

    bool ColorCoroutineRunning = false;

    IEnumerator FlashColor(Color c, float speed)
    {
        //Debug.Log("insdie other color thing1 and is called by " + BlockPosition);
        ColorCoroutineRunning = true;
        while (rend.material.color != c)
        {
            rend.material.color = Color.Lerp(rend.material.color, c, Time.deltaTime*speed);
            yield return new WaitForEndOfFrame();
        }
        rend.material.color = c;
        yield return new WaitForEndOfFrame();

        //Debug.Log("insdie other color thing3 and is called by " + BlockPosition);
        while (rend.material.color != LivingColor)
        {
            rend.material.color = Color.Lerp(rend.material.color, LivingColor, Time.deltaTime*(speed/2));
            yield return new WaitForEndOfFrame();
        }
        rend.material.color = LivingColor;
        ColorCoroutineRunning = false;

        yield return null;
    }

    //######################################################################################################################################

    public void ForceReproduce(int count) //
    {
        for (int i = 0; i < count; i++)
        {
            if (DeadNeighbors.Count == 0 || CountNeighbors() == 8) return;
            Block ChildB = GetRandomNeighbor();
            if (CountNeighbors() >= 8 || DeadNeighbors.Count == 0) return;
            ChildB.ComeToLife(LivingColor, MaxHealth, MainParentBlock, IsEnemy, tag); 
        }

    }

    //######################################################################################################################################

    Block GetRandomNeighbor() // 12/22/2015, throwing division by zero error, which should be impossible....odd
    {
        return DeadNeighbors[(int)Random.Range(0, ((DeadNeighbors.Count - 1) % DeadNeighbors.Count))];
    }
    //######################################################################################################################################

    public void TakeImpactDamage(int Amount)
    {
        if ((CountNeighbors() < 6) || Tenacity < 2.5f) Health -= Amount;
    }

    public void SetReproRate(int Repro)
    {
        ReproductionChance = Repro;
        B_ReproThres = 150;
        Health += 200;
        MaxHealth = Mathf.Clamp(MaxHealth, 0, (int)Health);
    }


    public void Spin()
    {
        transform.Rotate(transform.up);
    }

    public void Flip()
    {
        transform.Rotate(transform.forward);
    }

    //######################################################################################################################################
    public void TakeImpact(int ImpactDamage, string sender)
    {
        StartCoroutine(GetBigger());
        if (sender != tag) TakeImpactDamage(ImpactDamage);
        else { }
    }

    //######################################################################################################################################

    //IEnumerator Impact(int value)
    //{
    //    while (calls < 2)
    //    {
    //        transform.position = Vector3.Lerp(transform.position, new Vector3(0, 0, -value), Time.deltaTime * 0.8f);
    //        calls++;
    //        yield return null;
    //    }

    //    while (calls != -10)
    //    {
    //        transform.position = Vector3.Lerp(transform.position, OriginalPosition, Time.deltaTime * 5f);
    //        calls--;
    //        yield return null;
    //    }
    //    calls = 0;
    //    transform.position = OriginalPosition;
    //    ImpactValueR = 0;
    //    StopCoroutine("Impact");

    //}



    IEnumerator GetBigger()
    {
        for (int i = 0; i < 3; ++i)
        {
            transform.localScale /= 1.05f;
            yield return new WaitForSeconds(0.02f);
        }

        for (int j = 0; j < 3; ++j)
        {
            transform.localScale *= 1.05f;
            yield return new WaitForSeconds(0.02f);
        }

        transform.localScale = originScale;
        yield return null;
    }
    

}
