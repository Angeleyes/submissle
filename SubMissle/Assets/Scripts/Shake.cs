﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour {

    float x, y;

    void ShakeObject()
    {
        x = Mathf.Clamp(x, 1, 8);
        y = Mathf.Clamp(y, 1, 8);
        StartCoroutine(Move(x, y));
    }

    IEnumerator Move(float x, float y)
    {
        int count = 10;
        while (count > 0)
        {
            yield return new WaitForSeconds(0.04f);
            transform.Translate(new Vector2(x, y) * Time.deltaTime);
            transform.Translate(new Vector2(y, x) * Time.deltaTime);
            x *= -1 * Random.Range(1.1f,1.3f);
            y *= -1 * Random.Range(1.1f, 1.3f);
            count--;
        }

    }

}
