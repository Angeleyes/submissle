﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShockWave : MonoBehaviour {

    internal SphereCollider BlastRaidus;
    public int Blast;
    public float ExpansionRate;
    public int ImpactDamage;
    Color Target;
    string Sender;
    private GameObject MainCamref;

    public enum Type
    {
        MissleBlast,
        PushBlast,
        DeathBlast,

    }

    public Type WaveType;

    public List<AudioClip> ImpactSounds;
    internal AudioSource ImpactSound;

    void Awake()
    {
        MainCamref = GameObject.FindGameObjectWithTag("MainCamera");
        //probably room for optimization here since the shockwave is created and destoryed so often....
        ImpactSound = GetComponent<AudioSource>();
        ImpactSound.PlayOneShot(ImpactSounds[Random.Range(0, ImpactSounds.Count - 1)]);
        BlastRaidus = GetComponent<SphereCollider>();
    }
	// Use this for initialization
	void Start ()
    {
        //MainCamref = GameObject.FindGameObjectWithTag("MainCamera");
	}


    public void Explode(Type ExplosionSource, string sender)
    {
        Sender = sender;
        MainCamref.SendMessage("ShakeObject", null, SendMessageOptions.DontRequireReceiver);
        StartCoroutine(TriggerBlast());

    }
	
    IEnumerator TriggerBlast()
    {
        
        while(BlastRaidus.radius < Blast)
        {
            //Debug.Log("expanding..." + BlastRaidus.radius);
            BlastRaidus.radius += ExpansionRate;
            //ExpansionRate++;
            //BlastRaidus.bounds.Expand(ExpansionRate);
            yield return null;
        }
        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);

    }


    public void OnTriggerEnter(Collider HitObject)
    {

        //Debug.Log("collide on shock called..");
        if (HitObject.gameObject.layer == 8) HitObject.gameObject.GetComponent<Block>().TakeImpact(ImpactDamage, Sender);
        //if (HitObject.gameObject is Block) HitObject.gameObject.GetComponent<Block>().TakeImpact(ShockImpact);
    }

}
