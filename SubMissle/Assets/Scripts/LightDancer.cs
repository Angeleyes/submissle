﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightDancer : MonoBehaviour {


    public List<Vector3> SlerpPoints;
    public float Speed;
    public float RotationSpeed;

    public float Boost = 20f;

    float[] Spectrum;

    public List<Light> Lights;

    float[] Specs;

    void Start()
    {
        Spectrum = new float[64];
        Specs = new float[5];
        StartCoroutine(Rotate());
    }

    void Update()
    {

        AudioListener.GetOutputData(Spectrum, 0);

        Specs[0] = Spectrum[2] + Spectrum[4];
        Specs[1] = Spectrum[12] + Spectrum[14];
        Specs[2] = Spectrum[22] + Spectrum[24];
        Specs[3] = Spectrum[32] + Spectrum[34];
        Specs[4] = Spectrum[57] + Spectrum[60];


        for(int i = 0; i<Lights.Count; ++i)
        {
            Lights[i].intensity = Mathf.Clamp(Specs[i%Specs.Length] * 12, 0.5f, 8);
        }


    }



    IEnumerator Rotate()
    {
        while(true)
        {
            transform.Rotate(Vector3.forward, 10f * Specs[4]);
            //transform.localRotation = Quaternion.Slerp(Quaternion.identity, Quaternion.AngleAxis(180f, Vector3.forward), Time.deltaTime);
            yield return null;

        }
    }


}
