﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AI : MonoBehaviour{


    public delegate void RAI();


    protected Vector2 BlocksPos;
    protected int BlocksCurrentHp;
    protected int NumberOfChildren;
    protected List<Vector2> MainsLocs;

    public Block ParentBlock;

    public List<AI> BehaviorList;

    protected int BehaviorID;
    private static int Identifier;
    //public void IncreaseReproductionRate() { ReproductionChance += 50; }
    //public void IncreaseSkill() { Skill += 2; }


    public void SetUpAI(List<Vector2> MainsLocations)
    {
        MainsLocs = MainsLocations;


    }

    public void AddNewAI(AI addition)
    {
        BehaviorList.Add(addition);
    }

    void Start()
    {
        BehaviorID = Identifier++;
    }

    public void SetParentBlock(Block b)
    {
        ParentBlock = b;
    }

    public void RunAI()
    {
        for (int i = 0; i < BehaviorList.Count; ++i)
        {
            BehaviorList[i] = Instantiate(BehaviorList[i], Vector2.zero, Quaternion.identity) as AI;
            BehaviorList[i].SetParentBlock(ParentBlock);
            BehaviorList[i].Run();
        }
        //foreach(AI ai in BehaviorList)
        //{
        //    ai.Run();
        //}

    }

    virtual public void Run()
    {

    }


}
