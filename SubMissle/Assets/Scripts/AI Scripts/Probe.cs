﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Probe : MonoBehaviour {


    //Note (CJ) : this script is used on a sphere collider that shoots out and is used by like 3/5 or 6 AI and...
    //How to use: drag n drop in inspector to w/e ai script wants it and assign the operation to do with seek via delgate

    Vector2 Target;
    float Speed = 0.1f;

    List<Block> PathList;
    Color PaintColor;

    public delegate void ProbeOperation(Block b);

    public ProbeOperation DoOperation;
    public ProbeOperation DoOperationHit;

    Protruder ParentAI;


    public void SetSpeed(int speed)
    {
        Speed = speed;
    }

    void Start() // active goes on and aoff so might need to use awake()
    {
        PathList = new List<Block>();
        Target = new Vector2(0, 0);
        transform.position = new Vector3(transform.position.x, transform.position.y, -1);     
        //SeekTarget(Target);
    }

    public void SetParent(Protruder p)
    {
        ParentAI = p;
    }

    public void SeekTarget(Vector2 T)
    {
        gameObject.SetActive(true);
        Target = T;
        StartCoroutine(Seek());

    }

    IEnumerator Seek()
    {
        //yield return new WaitForSeconds(3);
        while(transform.position.magnitude - Target.magnitude > 0.5f)
        {            
            transform.position = Vector3.Lerp(transform.position, new Vector3(Target.x, Target.y, -1), Speed * Time.deltaTime);
            yield return null;
        }

        ParentAI.ResetProbe(this);
    }


    void OnTriggerEnter(Collider c)
    {
        if(c.gameObject.tag == "Block")
        {
            Block hitBlock = c.gameObject.GetComponent<Block>();
            if (hitBlock.LivingColor == Color.black && !hitBlock.Dead)
            {
                DoOperationHit(hitBlock);
                PathList.Clear();
                ParentAI.ResetProbe(this);
                return;
            }
                    
            DoOperation(hitBlock);

            PathList.Add(c.gameObject.GetComponent<Block>());
        }

    }


    void OnTriggerExit(Collider c)
    {
        if (c.gameObject.tag == "Block")
        {
            
        }
    }




 






}
