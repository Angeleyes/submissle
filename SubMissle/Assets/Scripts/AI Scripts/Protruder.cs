﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Protruder : AI {

    //maybe do a require component of block 

    public Probe ProProbe;
    
    Queue<Probe> ProbeQue;

    public int ProbeSpawns = 2;

	void Start ()
    {

    }

    public void ProbeDoNothing(Block b)
    {

    }

    public void ProbePaintPath(Block b)
    {
        if (b is EnemyBlockMain) return;


        b.InitializeBlock(b.transform.position, Block.Btype.Block);
        b.ComeToLife(ParentBlock.LivingColor, 300, ParentBlock, ParentBlock.IsEnemy, ParentBlock.BlockTag);
        //b.SetReproRate(0);
        b.Bless();
        b.AttackSpeed *= 2;
        // set other things here like repro rate and hp  
    }

    public void ResetProbe(Probe p)
    {
        Debug.Log("reseting probe");
        p.transform.position = new Vector3(ParentBlock.BlockPosition.x, ParentBlock.BlockPosition.y, -1);
        p.gameObject.SetActive(false);
        ProbeQue.Enqueue(p);
    }


    //IEnumerator Protrude(Vector2 Target)
    //{
    //    SphereCollider Probe = Instantiate(GProbe, BlocksPos, Quaternion.identity) as SphereCollider;
    //    Probe.gameObject.SetActive(true);
    //    while (Probe.transform.position.magnitude - Target.magnitude > 0.5f && Probe.gameObject.active)
    //    {
    //        //Probe.transform = transform.Translate()


    //        //algorithm for feelers ai: Shoot small probe towards a randomly choosen main
    //        //while heading towards main, every block collision tag gets added to a list (this is the path)
    //        // if detected a collision with main (compare tag) then traverse list
    //        yield return null;
    //    }
    //}


    //TODO: think about setting the probes delegate instead of a direct function call?
    void ProtrudeTowards()
    {
        if (ProbeQue.Count > 0)
        {
            Vector3 target = GameManager.RandomPointAroundMain();
            ProbeQue.Dequeue().SeekTarget(target);
        }
        else Debug.Log("que is empty....waiting...");
    }

    public override void Run()
    {
        ProbeQue = new Queue<Probe>();
        Debug.Log("pro running....block paretn is.." + ParentBlock.BlockPosition);

        for (int i = 0; i < ProbeSpawns; ++i)
        {
            ProbeQue.Enqueue(Instantiate(ProProbe, ParentBlock.BlockPosition, Quaternion.identity) as Probe);
        }

        foreach (Probe p in ProbeQue)
        {
            p.gameObject.SetActive(false);
            p.SetParent(this);
            p.DoOperationHit += ProbeDoNothing;
            p.DoOperation += ProbePaintPath;
        }

        StartCoroutine(RunPro());
    }

    IEnumerator RunPro()
    {
        yield return new WaitForSeconds(2);
        ProtrudeTowards();
        yield return new WaitForSeconds(15);
        ProtrudeTowards();
        StartCoroutine(RunPro());
    }
}
