﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class MovieScript : MonoBehaviour
{


	public int ClipDuration;
	public string MovieName;


#if UNITY_ANDROID || UNITY_IOS


	void Awake()
	{
		this.gameObject.SetActive(false);
	}

#endif

#if UNITY_EDITOR_WIN || UNITY_EDITOR_OSX

    public MovieTexture MovieScene;
    public int WaitSecondsAtStart;

    private AudioSource MovieAudio;
    //public float Timer;

    RawImage img;

    void Start()
    {
        img = GetComponent<RawImage>();
        img.texture = MovieScene as MovieTexture;


        this.gameObject.SetActive(false);
        //Audio = GetComponent<AudioSource>();
    }

    public void SetandPlay(MovieTexture M, int TimeDelay)
    {
		
		this.gameObject.SetActive(true);
        MovieScene = M;
        img.texture = MovieScene;
        MovieAudio = GetComponent<AudioSource>();
        MovieAudio.clip = MovieScene.audioClip;

        WaitSecondsAtStart = TimeDelay;
        PlayMovie();
    }

    public void PlayMovie()
    {
        this.gameObject.SetActive(true);
        if (MovieAudio != null) MovieAudio.Play();
        StartCoroutine("PlayingMovie");
    }

    IEnumerator PlayingMovie()
    {
        yield return new WaitForSeconds(WaitSecondsAtStart);
        Debug.Log("Playing movie: " + MovieScene.name);
        MovieScene.Play();
        //Audio.Play();
        yield return new WaitForSeconds(MovieScene.duration);
        //gameObject.SetActive(false);
        img.CrossFadeAlpha(0, 1, true);
        Debug.Log("turning movie off");
        MovieScene.Stop();
        this.gameObject.SetActive(false);

    }

#endif

}

