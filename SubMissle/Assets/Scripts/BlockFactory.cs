﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockFactory : MonoBehaviour {


    protected static BlockFactory Instance;

    public GameObject NormalBlock;
    public List<GameObject> EnemyMainList;
    public GameObject YouMainBlock;

    void Start()
    {
        Instance = this;
        //EnemyMainList = new List<GameObject>();
    }

    public static Block CreateBlock(Vector2 pos, Block.Btype type)
    {
        GameObject block = Instantiate(Instance.NormalBlock, new Vector3(pos.x, pos.y, -1), Quaternion.identity) as GameObject;
        block.GetComponent<Block>().InitializeBlock(pos, type);

        Instance.DoConversion(block.GetComponent<Block>());
        return block.GetComponent<Block>();
    }

    public static BlockMain CreateMain(Vector2 pos, Block.Btype type)
    {
        GameObject block = Instantiate(Instance.YouMainBlock, new Vector3(pos.x, pos.y, -1), Quaternion.identity) as GameObject;
        block.GetComponent<BlockMain>().InitializeBlock(pos, type);
        GameManager.Mains.Add(block.GetComponent<BlockMain>());
        Instance.DoConversion(block.GetComponent<BlockMain>());
        return block.GetComponent<BlockMain>();
    }

    public static EnemyBlockMain CreateEnemyMain(Vector2 pos, Block.Btype type, int ID)
    {
        GameObject block = Instantiate(Instance.EnemyMainList[ID], new Vector3(pos.x, pos.y, -1), Quaternion.identity) as GameObject;
        block.GetComponent<EnemyBlockMain>().InitializeBlock(pos, type);

        GameManager.GlobalEnemyMains.Add(block.GetComponent<EnemyBlockMain>());
        Instance.DoConversion(block.GetComponent<EnemyBlockMain>());
        return block.GetComponent<EnemyBlockMain>();
    }

    private void DoConversion(Block b)
    {       
        //SendMessage("ConvertBlock", b, SendMessageOptions.DontRequireReceiver);
        GameManager.ConvertBlock(b);
    }

    //public static Block InstantiateConversion(Block b)
    //{
    //    if (b.GetComponent<BlockMain>() != null) return CreateMain(null, b.BlockPosition, Block.Btype.YouMain);
    //    else if (b.GetComponent<EnemyBlockMain>() != null) return CreateEnemyMain(null, b.BlockPosition, Block.Btype.EMain);
    //    else return CreateBlock(b.BlockPosition, Block.Btype.Block);
    //}


}
