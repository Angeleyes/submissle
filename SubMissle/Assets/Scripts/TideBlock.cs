﻿using UnityEngine;
using System.Collections;

public class TideBlock : Block
{


    int Timer = 0;


    public Color FirstColor;
    public Color SecondColor;

    // Use this for initialization
    override public void Start()
    {
        Dead = false;
        Health = 999;
        MaxHealth = (int)Health;

        StartCoroutine("ColorFade");
    }

    IEnumerator ColorFade() 
    {
        while (Timer*Time.deltaTime < 5)
        {
            rend.material.color = Color.Lerp(rend.material.color, SecondColor, 0.5f*Time.deltaTime);
            Timer++;
            yield return null;
        }

        Timer = 0;
        StartCoroutine("UnColorFade");
    }

    IEnumerator UnColorFade()
    {
        while (Timer * Time.deltaTime < 10) // fadingintrue
        {
            rend.material.color = Color.Lerp(rend.material.color, FirstColor, 0.5f*Time.deltaTime);
            Timer++;
            yield return null;
        }
        Timer = 0;
        StartCoroutine("ColorFade");
    }


    public void TideComeIn()
    {
        StartCoroutine("TideRising");
    }

    IEnumerator TideRising()
    {
        //stack push pop or recusion
        yield return new WaitForSeconds(1);

    }


}