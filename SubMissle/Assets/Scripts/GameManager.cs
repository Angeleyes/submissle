﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    protected static GameManager GM;
    //public float CurtainTimer;

    public Button SubmissleActivate;

    public static int EnemyBlockID = -1;
    public static int ID = 0;  

    public static bool GameStarted = false;
    public static bool MainBlockAlive = false;
    public int StartTime;

    public BlockMain YouMain; // make list later
    static BlockMain You;
    static EnemyBlockMain NotYou;
    public EnemyBlockMain EnemyMain;
    public List<Vector3> EnemyMainPosList;
    public List<EnemyBlockMain> Enemymains;

    //inspector variables
    //public int ReproductionMax; // 650
    //public int HealthLimit; // 500
    //public float SpiritRate; // rate in which spirit increases  0.01
    //public float HealthLossRate; // rate in which hp is lost each cycle 0.025 (change from 0.025 to 1)
    //public int HealthBoost; // 50 atm
    //public int HealingTouch; // amount healed when you touch/tap a block 2
    //public int TenacityLimit; // thres at which a block will die if gives birth
    //public int ReproductionThreshold; // reproduction threhold to pass inorder to trigger a reproduce 200


    public int PushPrice = 75;

    public static List<Block> BlockUpdateList;
    private Queue<Block> BlockUpdateQue;

    public static int YouBlockTotal = 0;

    public Block BlockObject;
    public Block EmptyBlocks;
    public TideBlock Tide;

    public static List<SubMissle> Missles;
    public static bool MissileMode;
    public static bool TogglePushMode;

    public static List<int> LevelDataValues;

    public GameObject BackGround;
    public AudioSource Music;
    public AudioClip MusicClip;

    public int NumberOfTotalBlocks;
    public int BlockPopulation;
    //public int RedBlockPopulation;
    public int BoardSize;

    public static int TotalFights;

    internal int BlockHealth;
    internal int SuperBlockHP;
    internal int MainBlockHP;

    public static Dictionary<Vector2, Block> BlockPositionList;

    public static bool MainBlockExists = false;
    public static bool NotYouMainExists = false;

    public float yDelim = 0.25f;
    public float xDelim = 0.25f;

    internal float i;
    internal float j;

    public bool SubButtonOn = false;
    public bool PushModeOn = false;
    public bool ShowMovie = false;

    public GameObject BlockParent;

    public static float MainTimer;

    public Camera MainCamera;
    public static Queue<SubMissle> MissleMag;

    internal TouchControl GameControls;

    public static List<BlockMain> Mains;
    public static List<EnemyBlockMain> GlobalEnemyMains;

    public GameObject MovieScreen;


    public static int StartTimeref;
    public delegate void GameOver();
    public static event GameOver GameWin;
    public static event GameOver GameLost;

    void Awake()
    {
        GM = this;

        BlockMain.OnMainDeath += SignalMainDeath;
        EnemyBlockMain.OnEnemyMainDeath += SignalEnemyMainDeath;

        You = YouMain;
        NotYou = EnemyMain;
        BoardSize *= 2;
        YouBlockTotal = 0;
        MainTimer = 0;
        
        Mains = new List<BlockMain>();
        GlobalEnemyMains = new List<EnemyBlockMain>();


        MainCamera = MainCamera.GetComponent<Camera>();
        GameControls = MainCamera.GetComponent<TouchControl>();

		MissleMag = new Queue<SubMissle>();
        foreach(SubMissle s  in FindObjectsOfType<SubMissle>()) // check this...possible bug
        {
            if(s.gameObject.tag == "Submissle") MissleMag.Enqueue(s);         
        }

        StartTimeref = StartTime;
        BlockUpdateList = new List<Block>();
        BlockPositionList = new Dictionary<Vector2, Block>();
        SetUpLevel(1);

        StartCoroutine(UpdateBlockList());

    }

    void Start() 
    {
		if (ShowMovie)
		{
#if UNITY_ANDROID || UNITY_IOS
            StartTime = (int)MovieScreen.GetComponent<MovieScript>().MovieScene.duration;
			Handheld.PlayFullScreenMovie(MovieScreen.GetComponent<MovieScript>().MovieName, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.Fill);
#endif

#if UNITY_EDITOR_WIN || UNITY_EDITOR_OSX
            MovieScreen.SetActive(true);
            MovieScreen.GetComponent<MovieScript>().PlayMovie();
#endif

		}
        
        if (SubButtonOn) SubmissleActivate.gameObject.SetActive(true);
        else SubmissleActivate.gameObject.SetActive(false);

        if (PushModeOn) TogglePushMode = true;
       
    }

    void OnGUI()
    {
        GUI.Label(new Rect(new Vector2(0, 0), new Vector2(300, 50)), MainTimer.ToString());
        GUI.Label(new Rect(new Vector2(0, 15), new Vector2(300, 50)), YouBlockTotal.ToString());
        GUI.Label(new Rect(new Vector2(0, 30), new Vector2(300, 50)), (YouBlockTotal*0.15f).ToString());
        GUI.Label(new Rect(new Vector2(0, 60), new Vector2(300, 50)), "Missle Mode: " + MissileMode.ToString());
        GUI.Label(new Rect(new Vector2(0, 75), new Vector2(300, 50)), "Update List Size: " + BlockUpdateList.Count.ToString());
        //GUI.Label(new Rect(new Vector2(0, 75), new Vector2(300, 50)), MissleMag.Count.ToString());
    }

    public void SetMissleMode(bool Setting)
    {
        MissileMode = GameControls.ToggleTouchMode(Setting);
    }

    int temp = 0;

    void Update()
    {
        //if (MainTimer > LevelIntro.duration + 2) MovieScreen.gameObject.SetActive(false);
        //if(BlockPopulation >= 600) Missles.Add(new SubMissle(ref MissleBlastRadius, MissleHangTime, MissleSpeed));
        MainTimer += Time.deltaTime;

        foreach(BlockMain b in Mains)
        {
            if (b == null) break;
            temp += b.ChildrenPopulation;  //BUG here, exception
            YouBlockTotal = temp;
            temp = 0;
        }

        if (MainTimer > StartTime)
        {
            GameStarted = true;
            StartTimeref = 0;
        }
 
        if(YouBlockTotal > PushPrice)
        {
            PushPrice *= 2;
            BlockMain.MainPushes++;
        }


        //if(MainTimer >= LevelScript.LevelTrigger[i].TriggerTime)
        //{
        //LevelScript.ExecuteEvent(i);
        //i++;
        //}
    }



    IEnumerator UpdateBlockList()
    {
        Block[] Btemp = new Block[BlockUpdateList.Count];
        BlockUpdateList.CopyTo(Btemp);

        foreach (Block block in Btemp)
        {
            block.BlockUpdate();
        }
        yield return new WaitForEndOfFrame();

        StartCoroutine(UpdateBlockList());

    }

    public static void ShootMissle(Vector2 Position, string sendertag)
    {
        Debug.Log("calling shoot missle");
        SubMissle S = MissleMag.Dequeue();
        S.Launch(Position, sendertag);
        MissleMag.Enqueue(S);
    }


    void SetUpLevel(int LevelType) 
    {
        for (i = -xDelim*BoardSize; i < xDelim * (BoardSize + 1); i += xDelim)
        {
            for (j = -yDelim*BoardSize; j < yDelim * (BoardSize + 1); j += yDelim)
            {
                Block Empty0 = Instantiate(EmptyBlocks, new Vector3(i, j, -1), Quaternion.identity) as Block;

                Empty0.transform.parent = BlockParent.transform;

                Empty0.BlockPosition = new Vector2(i, j);
                Empty0.Dead = true;
                BlockPositionList.Add(Empty0.BlockPosition, Empty0);
                NumberOfTotalBlocks++;
            }
        }

        // creates the edge of the grid
            for(int l = 0; l < (BoardSize +1)*2; l++)
            {

                TideBlock TopTide = Instantiate(Tide, new Vector3((i-xDelim) - (xDelim*l), j, -1), Quaternion.identity) as TideBlock; // top
                TopTide.BlockPosition = new Vector2((i - xDelim) - (xDelim * l), j);
                TideBlock RightTide = Instantiate(Tide, new Vector3(i, (-j+xDelim) + (xDelim*l), -1), Quaternion.identity) as TideBlock; // right
                RightTide.BlockPosition = new Vector2(i, (-j + xDelim) + (xDelim * l));
                TideBlock BottomTide = Instantiate(Tide, new Vector3((-i + xDelim) + (xDelim * l), -j, -1), Quaternion.identity) as TideBlock; // bottom
                BottomTide.BlockPosition = new Vector2((-i + xDelim) + (xDelim * l), -j);
                TideBlock LeftTide = Instantiate(Tide, new Vector3(-i, (j - xDelim) - (xDelim * l), -1), Quaternion.identity) as TideBlock; // left
                LeftTide.BlockPosition = new Vector2(-i, (j - xDelim) - (xDelim * l));

                TopTide.transform.parent = BlockParent.transform;
                RightTide.transform.parent = BlockParent.transform;
                BottomTide.transform.parent = BlockParent.transform;
                LeftTide.transform.parent = BlockParent.transform;

                //commented out on 12/22/2015,  add the tides to their own map or lists
                BlockPositionList.Add(TopTide.BlockPosition, TopTide);
                BlockPositionList.Add(RightTide.BlockPosition, RightTide);
                BlockPositionList.Add(BottomTide.BlockPosition, BottomTide);
                BlockPositionList.Add(LeftTide.BlockPosition, LeftTide);              
            }

            //traverse the dictionary and setup each block
            //this foreach statement sets up the neighbor blocks of the instantiated empty0 block
            foreach (KeyValuePair<Vector2, Block> Item in BlockPositionList)
            {
                try
                {
                Item.Value.Dead = true; // all empty blocks start off as dead
                Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x + xDelim, Item.Value.BlockPosition.y)); // to the right
                Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x - xDelim, Item.Value.BlockPosition.y)); // to the left
                Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x, Item.Value.BlockPosition.y + yDelim)); // above
                Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x, Item.Value.BlockPosition.y - yDelim)); // below


                Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x + xDelim, Item.Value.BlockPosition.y + yDelim)); // upper right corner
                Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x - xDelim, Item.Value.BlockPosition.y + yDelim)); // upper left corner
                Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x + xDelim, Item.Value.BlockPosition.y - yDelim)); // bottom right corner
                Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x - xDelim, Item.Value.BlockPosition.y - yDelim)); // bottom left corner

                foreach (Vector2 v in Item.Value.NeighborPosList)
                {
                    if (Item.Value is TideBlock) { }
                    else
                    {
                        if (BlockPositionList[v] is TideBlock) { }
                        else Item.Value.Neighbors.Add(BlockPositionList[v]);
                    }
                }

                } // end of try
                catch
                {
                    Debug.Log("trying to add neighbor: " + Item.Key + "....." + Item.Value.BlockPosition);
                }
        }


        //set up initial mains and enemy mains

        //sets up initial enemy mains

        ChangeBlock(YouMain, new Vector2(0, 0));

        for(int t = 0; t < Enemymains.Count; t++)
        {
            Enemymains[t].BlockPosition = EnemyMainPosList[t];
            ChangeBlock(Enemymains[t], Enemymains[t].BlockPosition);
            Debug.Log("t: " + t + " with entity name: " + Enemymains[t].gameObject.name);
            //BlockFactory.CreateEnemyMain(Ev, Block.Btype.EMain, 0); // change to for each with the enemy main list 

        //BUG: calling blockfactory methods causes a huge bug that turns off the gm script during run time WTF???
        }

        //BlockFactory.CreateMain(new Vector2(0, 0), Block.Btype.YouMain);
        //I want to use this line of code above, but cant because of major bug stated above.

    } // end of setup level


    public static void ChangeBlock(Block B, Vector2 pos)
    {
        List<Block> n = new List<Block>();

        n = BlockPositionList[pos].Neighbors;

        if (B is BlockMain)
        {
            Block YouMain01 = Instantiate(You, new Vector3(pos.x, pos.y, -1), Quaternion.identity) as Block; //OMG this works, alright sweet
            MainBlockExists = true;
            YouMain01.Dead = true;
            YouMain01.BlockPosition = pos;

            foreach (Block b in BlockPositionList[pos].Neighbors) // checking if this fixed the BUG on 3/20/16
            {
                b.Neighbors.Remove(BlockPositionList[pos]);
                b.Neighbors.Add(YouMain01);
            }

            Destroy(BlockPositionList[pos].gameObject);
            BlockPositionList.Remove(pos);

            BlockPositionList.Add(YouMain01.BlockPosition, YouMain01);

            //Mains.Add(YouMain01);

            YouMain01.Neighbors = n;
        }

        else if (B is EnemyBlockMain)
        {
            EnemyBlockMain Em0 = Instantiate(B, new Vector3(pos.x, pos.y, -1), Quaternion.identity) as EnemyBlockMain;
            Em0.Dead = true;
            Em0.BlockPosition = pos;
            Em0.BlockAI = B.GetComponent<EnemyBlockMain>().BlockAI;

            foreach (Block b in BlockPositionList[pos].Neighbors) // checking if this fixed the BUG on 3/20/16, YUP it does 3/21/16
            {
                b.Neighbors.Remove(BlockPositionList[pos]);
                b.Neighbors.Add(Em0);
            }

            Destroy(BlockPositionList[pos].gameObject);
            BlockPositionList.Remove(pos);

            BlockPositionList.Add(pos, Em0);

            //EnemyMains.Add(Em0);

            Em0.Neighbors = n;
        }

    }



    public static Vector3 RandomPointAroundMain()
    {
        return Mains[0].BlockPosition; // might want to change the z to -1f
    }

    public static void ConvertBlock(Block b)
    {
        List<Block> n = new List<Block>();
        n = BlockPositionList[b.BlockPosition].Neighbors;

        foreach (Block bn in BlockPositionList[b.BlockPosition].Neighbors)
        {
            bn.Neighbors.Remove(BlockPositionList[b.BlockPosition]);
            bn.Neighbors.Add(b);
        }

        Destroy(BlockPositionList[b.BlockPosition].gameObject);
        BlockPositionList.Remove(b.BlockPosition);

        //b = BlockFactory.InstantiateConversion(b);

        BlockPositionList.Add(b.BlockPosition, b);
        b.Neighbors = n;

    }


    private void SignalMainDeath(BlockMain B)
    {
        Mains.Remove(B);
        if (Mains.Count == 0) GameEnd(2);
    }

    public void SignalEnemyMainDeath(EnemyBlockMain E) // this static shit, is a fuckin problem
    {
        GlobalEnemyMains.Remove(E);
        if (GlobalEnemyMains.Count == 0) GameEnd(1);
    }

    private void GameEnd(int type)
    {
        BlockMain.OnMainDeath -= SignalMainDeath;
        EnemyBlockMain.OnEnemyMainDeath -= SignalEnemyMainDeath;

        if (type == 1) PlayerWin();
        else if (type == 2) PlayerLose();
    }

    private void PlayerLose()
    {
        Debug.Log("PLAYER YOU LOST");
        if (GameLost != null) GameLost();
    }

    private void PlayerWin()
    {
        Debug.Log("Player wins");
        if (GameWin != null) GameWin();

        //move camera to positoin
        Invoke("ExplodeLevel", 3);

    }

    private void ExplodeLevel()
    {
        Vector3 explosionPos = new Vector3(0, 0, -2);
        Collider[] colliders = Physics.OverlapSphere(explosionPos, 10);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(40, explosionPos, 4, 2.0f, ForceMode.Impulse);


        }
    }




    public void BackToMenu()
    {
        //SceneManager.UnloadScene()
        Application.LoadLevel(0);
    }

} 
