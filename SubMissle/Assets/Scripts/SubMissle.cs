﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SubMissle : MonoBehaviour {



    // need to parse out block radius into the 4 correct waves, gona be hard to get correct positions....well not really

    //Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x + xDelim, Item.Value.BlockPosition.y)); // to the right
    //Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x - xDelim, Item.Value.BlockPosition.y)); // to the left
    //Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x, Item.Value.BlockPosition.y + yDelim)); // above
    //Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x, Item.Value.BlockPosition.y - yDelim)); // below


    //Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x + xDelim, Item.Value.BlockPosition.y + yDelim)); // upper right corner
    //Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x - xDelim, Item.Value.BlockPosition.y + yDelim)); // upper left corner
    //Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x + xDelim, Item.Value.BlockPosition.y - yDelim)); // bottom right corner
    //Item.Value.NeighborPosList.Add(new Vector2(Item.Value.BlockPosition.x - xDelim, Item.Value.BlockPosition.y - yDelim)); // bottom left corner

    // FOR WAVE 2: then take corners and

    // UPPER LEFT : its position -ydelim, and its position +xdelim
    // UPPER RIGHT: its pos -xdelim and -ydelim
    // BOTTOM LEFT: its pos +ydelim, and +xdelim
    // BOTTOM RIGHT: its pos +ydelim, and -xdelim



    //FOR WAVE 3: SAME AS WAVE 2 BUT *3 AND THE CORNERS ARE *2

    //UPPER LEFT: its position -ydelim, -ydelim*2, +xdelim, and +xdelim*2
    //UPPER RIGHT: its pos -xdelim, -xdelim*2, -ydelim, and -ydelim*2
    //BOTTOM LEFT: its pos +ydelim, +ydelim*2, +xdelim, and +xdelim*2
    //BOTTOM RIGHT: its pos +ydleim, +ydelim*2, -xdelim, and -xdelim*2


    //FOR WAVE 4: SAME BUT *4 AND CORNERS ARE *2 and *3

    //UPPER LEFT: its position -ydelim, -ydelim*2, -ydelim*3, +xdelim, +xdelim*2, and +xdelim*3
    //UPPER RIGHT: its pos -xdelim, -xdelim*2, -xdelim*3, -ydelim, -ydelim*2, -ydelim*3
    //BOTTOM LEFT: its pos +ydelim, +ydelim*2, +ydelim*3, +xdelim, +xdelim*2, +xdelim*3
    //BOTTOM RIGHT: its pos +ydleim, +ydelim*2, +ydelim*3, -xdelim, -xdelim*2, -xdelim*3

    //Block[] Wave1; // wave 1 consists of the neighbor list of the target block...aka all delims are *1
    //Block[] Wave2; // wave 2 consists of the same neighbor co-ords, but its all delims *2...then u take the corners and get the ones next to them
    //Block[] Wave3; // wave 3 is the same as wave 2 but iwth all delims *3
    //Block[] Wave4;

    public int Speed;
    Vector3 TargetPos;
    Rigidbody Body;
    bool Team;

    public ShockWave MissleShockWave;

    Vector3 Direction;
    Vector3 OriginalPos;
    public List<AudioClip> FlyingSounds;
    internal AudioSource FlyingSound;

    public Block.Btype BType;
    string Sender;

    //public delegate void AttackFormation(SubMissle S);
    //public AttackFormation Form;

    void Awake()
    {

        FlyingSound = GetComponent<AudioSource>();

        OriginalPos = transform.localPosition;
        Body = GetComponent<Rigidbody>();
    }

	void Start () 
    {
        //MissleShockWave = gameObject.GetComponent<ShockWave>();
	}	

    public void Launch(Vector3 Target, string SenderTag)
    {
        GameManager.MissileMode = false;
        Sender = SenderTag;
        FlyingSound.clip = FlyingSounds[Random.Range(0, FlyingSounds.Count - 1)];
        FlyingSound.Play();
        TargetPos = Target;
        transform.LookAt(TargetPos);
        Body.AddForce((TargetPos - transform.position) * Speed * Time.smoothDeltaTime);
        //Debug.Log("launching missle " + this.name);
        StartCoroutine(WaitForReset());

        //possible solution to multiple blocks being hit would be to call sw.explode up here with a timed delay of like 1s
    }

    private void SpawnShock()
    {
        ShockWave Sw = Instantiate(MissleShockWave, TargetPos, Quaternion.identity) as ShockWave;
        BlockFactory.CreateMain(TargetPos, Block.Btype.YouMain); // this script can now take in a block type for this line
        Sw.Explode(ShockWave.Type.MissleBlast, Sender);

    }
    private void ResetMissle()
    {
        Body.angularVelocity = Vector3.zero;
        Body.velocity = Vector3.zero;
        transform.localPosition = OriginalPos;
    }

    //public void OnTriggerEnter(Collider Hit) // TODO: make so it doesn't hit multiple blocks
    //{
    //    if(Hit.GetComponent<Block>() != null) // TODO: change later to layermask
    //    {           

    //    }
    //}

    IEnumerator WaitForReset()
    {
        yield return new WaitForSeconds(0.6f);

        SpawnShock();
        ResetMissle();
    }

    IEnumerator Fire()
    {
        while(transform.position != TargetPos)
        {
            Direction = TargetPos - transform.position;
            Direction.Normalize();
            transform.Translate(Direction*Speed, Space.World);
            yield return null;
        }

        Body.AddForce((TargetPos - transform.position) * Speed * Time.smoothDeltaTime);
        

        MissleShockWave = Instantiate(MissleShockWave, transform.position, Quaternion.identity) as ShockWave;
        transform.position = OriginalPos;
        MissleShockWave.StartCoroutine("Explode");
        StopCoroutine("Fire");
    }

}
