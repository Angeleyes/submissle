﻿using UnityEngine;
using System.Collections;

public class MusicFader : MonoBehaviour {

    public float FadeSpeed = 0.5f;

    AudioSource Music;
	public float OriginalVolume;


	public enum MusicDirection
	{
		MusicUp = 1,
		MusicDown = -1
	}


    public void FadeOutMusic()
    {
        StartCoroutine(Fade(0, MusicDirection.MusicDown));
    }

    IEnumerator Fade(float TargetLevel, MusicDirection Md)
    {
       // yield return new WaitForSeconds(0.3f);
        while (Music.volume != TargetLevel)
        {
			Music.volume += (0.1f*(int)Md);
            yield return new WaitForSeconds(FadeSpeed);
        }

    }

	public void FadeInMusic()
	{

		StartCoroutine(Fade(OriginalVolume, MusicDirection.MusicUp));
	}


    void Start()
    {
        Music = GetComponent<AudioSource>();
		OriginalVolume = Music.volume;

    }






}
