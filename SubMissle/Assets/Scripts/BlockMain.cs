﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockMain : Block   {

    //internal ShockWave MainPushShock;

    public ShockWave MainShockWave;
    public static int MainPushes = 0;
    internal List<Block> ChildList;
    public delegate void BlockMainHandler(BlockMain B);
    public static event BlockMainHandler OnMainDeath;


    public static void MainDeath(BlockMain B)
    {
        if (OnMainDeath != null) OnMainDeath(B);
    }

    override public void Start ()
    {
        Invoke("WakeMain", GameManager.StartTimeref);
	}

    void OnGUI()
    {
        GUI.Label(new Rect(new Vector2(0, 45), new Vector2(300, 50)), "MPushes:" + MainPushes.ToString());
    }
	

    public void WakeMain()
    {
        MainPushes = 0;
        BlockTag = "Block";
        //#
        BlockID = GameManager.ID++;

        MainParentBlock = this;
        Dead = false;
        IsEnemy = false;
        InitializeNeighbors();
        rend.material.color = Color.green;
        Blessed = true;
        
        Health = 1000;
        Tenacity = 7f;
        MaxHealth = (int)Health;
        ReproductionChance = 450;
        Skill = 5;
        //##
        GameManager.MainBlockAlive = true;
        GameManager.Mains.Add(this);

        LivingColor = Color.black;

        GameManager.BlockUpdateList.Add(this);
    }

    public override void Touched()
    {
        if(MainPushes > 0 && GameManager.TogglePushMode)
        {
            MainPushes--;
            ShockWave Wave = Instantiate(MainShockWave, transform.position, Quaternion.identity) as ShockWave;
            Wave.Explode(ShockWave.Type.MissleBlast, BlockTag);  
        }

    }

    public override void Die()
    {
        base.Die();

        ShockWave s = Instantiate(MainShockWave, transform.position, Quaternion.identity) as ShockWave;
        s.Explode(ShockWave.Type.DeathBlast, tag);
        //#
        //GameManager.SignalMainDeath(this);
        MainDeath(this);
        MainPushes = 0;
        BlockFactory.CreateBlock(BlockPosition, Btype.Block);

        

        
    }
}
