﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TitleBackGround : MonoBehaviour {

    //public TitleBlock BlockObject;
    public TitleBlock EmptyBlocks;
    public int BoardSize = 4;

    public float yDelim = 0.25f;
    public float xDelim = 0.25f;

    public float SlerpRotationSpeed = 0.01f;

    public AudioSource clip;
    public AudioSource clip2;
    public GameObject BlockParent;
    public Dictionary<Vector2, TitleBlock> BlockPositionList;

	void Start () 
    {

        BlockPositionList = new Dictionary<Vector2, TitleBlock>();
        SetUpLevel(1);
        StartCoroutine("Thunder1");

	}

    IEnumerator Thunder1()
    {
        yield return new WaitForSeconds(10);
        clip.Play();
        StartCoroutine("Thunder2");
    }

    IEnumerator Thunder2()
    {
        yield return new WaitForSeconds(10);
        clip2.Play();
        StartCoroutine("Thunder1");
    }

    void SetUpLevel(int LevelType)
    {
        for (float i = -xDelim * BoardSize; i < xDelim * (BoardSize + 1); i += xDelim)
        {
            for (float j = -yDelim * BoardSize; j < yDelim * (BoardSize + 1); j += yDelim)
            {
                TitleBlock Empty0 = Instantiate(EmptyBlocks, new Vector3(i, j, Random.Range(-0.9f, -0.82f)), Quaternion.identity) as TitleBlock;
                Empty0.transform.parent = BlockParent.transform;
                Empty0.BlockPosition = new Vector2(i, j);
                BlockPositionList.Add(Empty0.BlockPosition, Empty0);
                //need to setup edge cases...haha...

            }
        }

    }

	
	// Update is called once per frame
	void Update () 
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.AngleAxis(12, new Vector3(0, 1, 0)), Time.deltaTime * SlerpRotationSpeed);
	}
}
