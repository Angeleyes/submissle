﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyBlockMain : Block {


    //public List<AI> AiList;
    public Color BlockColor;
    public ShockWave DeathWave;

    public AI BlockAI;


    public delegate void EnemyMainHandler(EnemyBlockMain E);

    public static event EnemyMainHandler OnEnemyMainDeath;

    public static void EnemyMainDeath(EnemyBlockMain E)
    {
        if (OnEnemyMainDeath != null) OnEnemyMainDeath(E);
    }


    override public void Start ()
    {
        Invoke("SpawnEnemyMain", GameManager.StartTimeref);
	}
	

    void SpawnEnemyMain()
    {
        tag = BlockTag = "EnemyBlock";
        BlockID = GameManager.EnemyBlockID--;
        MainParentBlock = this;
        InitializeNeighbors();
        rend.material.color = LivingColor;
        Dead = false;
        //NotYouMainExists = true;
        Health = 20000;
        IsEnemy = true;
        MaxHealth = (int)Health;
        Tenacity = 5f;
        ReproductionChance = 400;
        Skill = 5;
        LivingColor = BlockColor; // fix this color shit later, its like backwards and fucked up
        GameManager.GlobalEnemyMains.Add(this);
        GameManager.BlockUpdateList.Add(this);
        //AiList[0] = Instantiate(, Vector3.zero, Quaternion.identity) as Protruder;

        if (BlockAI != null)
        {
            BlockAI = Instantiate(BlockAI, BlockPosition, Quaternion.identity) as AI;

            BlockAI.SetParentBlock(this); // can combine these into 1 function call
            BlockAI.RunAI();
        }

    }

    public override void Touched()
    {
        Debug.Log("enemy main touched hp is: " + Health);
    }

    override public void Die()
    {

        Debug.Log("calling enemy main die");
        base.Die();

        if (BlockAI != null)
        {
            foreach (AI ai in BlockAI.BehaviorList)
            {
                Destroy(ai.gameObject);
            }
        }

        ShockWave S = Instantiate(DeathWave, BlockPosition, Quaternion.identity) as ShockWave;
        S.Explode(ShockWave.Type.DeathBlast, BlockTag);

        BlockFactory.CreateBlock(BlockPosition, Btype.Block);
        //GameManager.SignalEnemyMainDeath(this);
        EnemyMainDeath(this);
    
    }
}
