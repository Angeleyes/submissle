﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LvlComplete : MonoBehaviour {


    //RawImage WinScreen;
    RawImage LoseScreen;

    RawImage[] R;

    Canvas[] C;
    Canvas WinScreen;

    Vector3 WinPosition = new Vector3(-4.38f, -5.65f, 2.81f);
    float MoveSpeed = 2;

    void Awake()
    {
        R = GetComponentsInChildren<RawImage>(true);
        LoseScreen = R[1];

        C = GetComponentsInChildren<Canvas>(true);
        WinScreen = C[1];

        LoseScreen.gameObject.SetActive(false);
        //WinScreen.gameObject.SetActive(false);
        GameManager.GameWin += EndWin;
        GameManager.GameLost += EndLose;
    }


    void EndWin()
    {
        //MoveToWinPosition();
        //DisplayWinScreen();
        Invoke("MoveToWinPosition", 2);
        Invoke("DisplayWinScreen", 2.5f);
        GameManager.GameWin -= EndWin;
        GameManager.GameWin -= EndLose;
        GameManager.GameStarted = false;
    }
    
    void RestartLevel()
    {
        
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.UnloadScene(scene.buildIndex);
        //SceneManager.SetActiveScene(scene);
        SceneManager.LoadScene(scene.buildIndex);
        GameManager.MainTimer = 0;
        GameManager.GameStarted = false;
        //SceneManager.LoadScene(scene.name);
    }

    void EndLose()
    {
        LoseScreen.gameObject.SetActive(true);
        LoseScreen.CrossFadeAlpha(1, 2, false);
        GameManager.GameWin -= EndLose;
        GameManager.GameWin -= EndWin;
        Invoke("RestartLevel", 6);
    }

    void DisplayWinScreen()
    {

        WinScreen.gameObject.SetActive(true);

    }

    void MoveToWinPosition()
    {
        this.GetComponent<Camera>().orthographicSize = 1.1f;
        //transform.localEulerAngles.Set(320.81f, 89.40f, 0.170f);
        //transform.rotation.SetEulerAngles(320.81f, 89.40f, 0.170f);
        transform.rotation = Quaternion.Euler(320.81f, 89.40f, 0.170f);
        StartCoroutine(MoveCamToWinPos());       
    }


    IEnumerator MoveCamToWinPos()
    {
        while (Vector3.Distance(transform.position, WinPosition) > 0.01f)
        {

            transform.position = Vector3.Lerp(transform.position, WinPosition, MoveSpeed * Time.deltaTime);
            yield return null;
        }
    }

}
